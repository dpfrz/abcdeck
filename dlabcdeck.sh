#!/bin/bash

i=0
while read p; do

	if [[ "$p" =~ ^http.* ]]; then
		#echo "$p"

		#echo "$i-$(echo "$p" | awk -F/ '{print $NF}')"

		curl -sL "$p" > $(echo "$i-$(echo "$p" | awk -F/ '{print $NF}')")

		((i=i+1))
	fi

done < abcdeck.m3u8

